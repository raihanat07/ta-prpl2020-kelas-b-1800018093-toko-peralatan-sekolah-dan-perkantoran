-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 14 Jul 2020 pada 16.24
-- Versi server: 10.1.38-MariaDB
-- Versi PHP: 7.3.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `toko_peralatan`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `barang`
--

CREATE TABLE `barang` (
  `no` int(11) NOT NULL,
  `id_barang` varchar(20) NOT NULL,
  `nama_barang` varchar(30) NOT NULL,
  `harga_barang` int(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `barang`
--

INSERT INTO `barang` (`no`, `id_barang`, `nama_barang`, `harga_barang`) VALUES
(0, '001', 'pena', 5000),
(0, '002', 'rautan', 3500),
(0, '003', 'penghapus', 2000),
(0, '004', 'spidol', 11000),
(0, '005', 'kertas folio kiki (200 lembar)', 24000),
(0, '006', 'kertas folio sidu (200 lmbr)', 25000),
(0, '007', 'kertas buffalo (100 lmr)', 20000),
(0, '008', 'kertas HVS A4 (100 lmbr)', 39500),
(0, '009', 'kertas folio bergaris (250 lem', 200000),
(0, '010', 'amplop polos (dus)', 30000),
(0, '011', 'buku tulis ABC', 8000),
(0, '012', 'buku kuitansi', 5000),
(0, '013', 'blangko', 18000),
(0, '015', 'stop map', 2000),
(0, '016', 'map diamond', 4000),
(0, '017', 'odner folio', 20000),
(0, '018', 'ballpoint (lusin)', 70000),
(0, '019', 'boxy pen (lusin)', 110000),
(0, '020', 'Tinta Parker', 50000),
(0, '021', 'Stabillo Boss', 75000),
(0, '022', 'Karet Penghapus ', 4000),
(0, '023', 'Cover (pak)', 75000),
(0, '024', 'Lack Band Hitam (roll)', 12000),
(0, '025', 'penggaris besi 1 m', 50000),
(0, '026', 'Stapler No. 17', 45000),
(0, '027', 'Isi Stapler No. 17 (dos)', 25000),
(0, '028', 'Penyekat Buku', 30000),
(0, '029', 'Lem Kertas', 13000),
(0, '030', 'Karbon Folio (dus)', 90000),
(0, '031', 'Tip-Ex Cair', 144000),
(0, '032', 'Tip-Ex Pentel ', 96000),
(0, '033', 'Tinta Stempel (tube)', 20000),
(0, '034', 'Bak Stempel ', 15000),
(0, '035', 'Gunting ', 20000),
(0, '036', 'tali rafia', 35000),
(0, '037', 'selotif besar', 20000),
(0, '038', 'selotif kecil', 20000),
(0, '039', 'pisau cutter', 20000),
(0, '040', 'Busur Derajat 0,5 m', 30000),
(0, '041', 'kalender', 40000),
(0, '042', 'buku agenda', 20000),
(0, '043', 'nota dinas', 9000),
(0, '044', 'rak buku', 20000),
(0, '046', 'notebook garis A4', 15000),
(0, '047', 'Double Tape', 16000),
(0, '048', 'Lack Band Bening', 18000),
(0, '049', 'Lack Band Coklat', 19000),
(0, '050', 'Stop Map Plastik (10 bh)', 25000),
(0, '051', 'Penghapus White Board ', 15000),
(0, '052', 'Paper Clips Besar (dus)', 15000),
(0, '053', 'Paper Clips Kecil (dus)', 10000);

-- --------------------------------------------------------

--
-- Struktur dari tabel `kasir`
--

CREATE TABLE `kasir` (
  `no` int(11) NOT NULL,
  `id_petugas_kasir` varchar(30) NOT NULL,
  `nama_petugas_kasir` varchar(30) NOT NULL,
  `alamat` text NOT NULL,
  `no_hp` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `kasir`
--

INSERT INTO `kasir` (`no`, `id_petugas_kasir`, `nama_petugas_kasir`, `alamat`, `no_hp`) VALUES
(0, '20001', 'maulana', 'bekasi', '082218762837'),
(0, '20002', 'fernando', 'lombok', '082376543267'),
(0, '20003', 'joni', 'sorogenen', '085654327865'),
(0, '20004', 'rani', 'jogja', '082287654576');

-- --------------------------------------------------------

--
-- Struktur dari tabel `pelanggan`
--

CREATE TABLE `pelanggan` (
  `no` int(4) NOT NULL,
  `id_pelanggan` varchar(20) NOT NULL,
  `nama_pelanggan` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `pelanggan`
--

INSERT INTO `pelanggan` (`no`, `id_pelanggan`, `nama_pelanggan`) VALUES
(0, '18001', 'saifudin'),
(0, '18002', 'revaldo azhar'),
(0, '18003', 'randa anwar '),
(0, '18004', 'rindu sari'),
(0, '18005', 'andi pustama'),
(0, '18006', 'udana');

-- --------------------------------------------------------

--
-- Struktur dari tabel `transaksi`
--

CREATE TABLE `transaksi` (
  `id_transaksi` int(30) NOT NULL,
  `id_pelanggan` varchar(30) NOT NULL,
  `id_barang` varchar(30) NOT NULL,
  `id_petugas_kasir` varchar(30) NOT NULL,
  `tanggal_transaksi` date NOT NULL,
  `jumlah_barang` int(11) NOT NULL,
  `total_harga` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `transaksi`
--

INSERT INTO `transaksi` (`id_transaksi`, `id_pelanggan`, `id_barang`, `id_petugas_kasir`, `tanggal_transaksi`, `jumlah_barang`, `total_harga`) VALUES
(23, '18001', '001', '20001', '2020-07-15', 2, 10000),
(24, '18001', '001', '20001', '2020-07-07', 4, 20000),
(25, '18003', '041', '20003', '2020-07-15', 2, 80000),
(26, '18005', '016', '20003', '2020-07-23', 3, 12000),
(27, '18003', '052', '20001', '2020-07-15', 5, 75000),
(28, '18004', '024', '20003', '2020-07-12', 8, 96000),
(29, '18006', '025', '20003', '2020-07-10', 2, 100000);

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `barang`
--
ALTER TABLE `barang`
  ADD PRIMARY KEY (`id_barang`);

--
-- Indeks untuk tabel `kasir`
--
ALTER TABLE `kasir`
  ADD PRIMARY KEY (`id_petugas_kasir`);

--
-- Indeks untuk tabel `pelanggan`
--
ALTER TABLE `pelanggan`
  ADD PRIMARY KEY (`id_pelanggan`);

--
-- Indeks untuk tabel `transaksi`
--
ALTER TABLE `transaksi`
  ADD PRIMARY KEY (`id_transaksi`),
  ADD KEY `id_pelanggan` (`id_pelanggan`) USING BTREE,
  ADD KEY `id_petugas_kasir` (`id_petugas_kasir`) USING BTREE,
  ADD KEY `id_barang` (`id_barang`) USING BTREE;

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `transaksi`
--
ALTER TABLE `transaksi`
  MODIFY `id_transaksi` int(30) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `transaksi`
--
ALTER TABLE `transaksi`
  ADD CONSTRAINT `transaksi_ibfk_1` FOREIGN KEY (`id_pelanggan`) REFERENCES `pelanggan` (`id_pelanggan`),
  ADD CONSTRAINT `transaksi_ibfk_2` FOREIGN KEY (`id_petugas_kasir`) REFERENCES `kasir` (`id_petugas_kasir`),
  ADD CONSTRAINT `transaksi_ibfk_3` FOREIGN KEY (`id_barang`) REFERENCES `barang` (`id_barang`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
