<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="hore.css">
    <title>
      TOKO PERALATAN SEKOLAH DAN PERKANTORAN </title>
  </head>

  <body     style="background-image:url(tokoo.jpg); background-size:cover; background-attachment: fixed;">


<div class="fixed-top" >
<nav class="navbar navbar-light" style="background-color: #56d0e2;">
  <div class="container">
  <a class="navbar-brand" href="#">WEBSITE TOKO PERALATAN SEKOLAH DAN PERKANTORAN</a>
  
  <div class="collapse navbar-collapse " id="navbarNavAltMarkup" style="background-color: #27b9dd">
    
</div>
</nav>
</div>

<div class="jumbotron "  style="height: 132px;" >  
  <div class="container " >
  <p class="display-4" align="center">Toko Peralatan Sekolah dan Kantor</p><!-- batas atas -->
<div style="background-color: rgba(25,0,0,0.9);">
<br>

   

         <ul style="color: white">
     <a href="menu.php" class="btn btn-secondary" align=center > kembali ke halaman utama</a> 
              
         

<h4 style="color: white" align="center">RIWAYAT TRANSAKSI</h4>
      <ul align=center style="padding-bottom: 150px;padding-top: 20px;padding-right: 100px;padding-left: 100px;">

        <thead class="thead-dark">
  <?php 
    include 'connect_db.php';
    ?>

    <br/><br/><br/>

    <form method="get">
      <label>Filter Tanggal</label>
      <input type="date" name="tanggal_transaksi">
      <input type="submit" value="filter">
    </form>

    <br/> <br/>

    <table border="1" class="table table-striped table-dark gray" style="margin-left: -80px;margin-right: 10px">
      <tr>
        <th> NO</th>
          <th>  ID TRANSAKSI </th>
          <th>  ID PELANGGAN </th>
          <th>  ID BARANG </th>
          <th>  ID PETUGAS KASIR </th>
          <th>  TANGGAL TRANSAKSI </th>
          <th>  JUMLAH BARANG </th>
          <th>  TOTAL HARGA </th>
          <th>  ACTION  </th>
      </tr>
      <?php 
      $no = 1;

      if(isset($_GET['tanggal_transaksi'])){
        $tanggal_transaksi = $_GET['tanggal_transaksi'];
        $sql = mysqli_query($connect,"select * from transaksi where tanggal_transaksi='$tanggal_transaksi'");
      }
      else
        {
        $sql = mysqli_query($connect,"select * from transaksi");
      }
      $total_harga=0; 
      while($data = mysqli_fetch_array($sql)){
      $total_harga += $data['total_harga']; 
      ?>
      <tr>
           <td><?php echo $no++; ?></td>
           <td><?php echo $data['id_transaksi']; ?></td>
           <td><?php echo $data['id_pelanggan']; ?></td>
           <td><?php echo $data['id_barang']; ?></td>
           <td><?php echo $data['id_petugas_kasir']; ?></td>
           <td><?php echo $data['tanggal_transaksi']; ?></td>
           <td><?php echo $data['jumlah_barang']; ?></td>
           <td><?php echo $data['total_harga']; ?></td>
      <td>
        <table><tr><td>
      <button type="button" class="btn btn-primary"><a href="updettransaksi.php?id_transaksi=<?php echo $data['id_transaksi']?>"style="color:white">Update</a></button></td>
      <td>
      <button type="button" class="btn btn-danger"><a href="p_hapusdatatransaksi.php?id_transaksi=<?php echo $data['id_transaksi']?>"style="color:white">Hapus</a></button></td>
        </tr>
        </table>
        
      </td>
        
      </tr>
      <?php 
      }
      ?>
  </tbody>
</table>
<h3 style="background-color: gray">Total Omset : Rp. <?php echo $total_harga ; ?></h3>
<br>

        



   </ul>
</div>

    
    
  

   
   
  </div>

   <!-- footer-->
<div style="background-color: #e3e5e8;margin-top: 8px;margin-left: -35px;margin-right: -100px;"  >
  <div class="card-header " ><h3 >more info</h3></div>
  <div class="card-body " align="center">
    <p class="card-text">
      <table width="80%;" style="margin-top: 0%">
      <tr >
        <th> LOKASI </th>
        <th>TENTANG KAMI</th>
         <th>LAINNYA</th>
      </tr>
      <tr>
        <td> jalan sorogenen, umbul harjo, yogyakarta</td>
        <td>melayani dengan sepenuh hati</td>
         <td>ikuti kami di web,istagram, dan facebook</td>
      </tr>
       </table>
    </p>
  </div>


<div class="card text-white bg-info mb-3" style="width: 100%">
    <div class="card-footer" ><h5  align="center" >&copy;Raihan Aqila Taufik - 1800018093 </h5 ></div>
  </div>
</div>
<!-- footer-->
</div>





    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
  </body>
</html> 
























